I'm adapting keycloak-nodejs-auth-utils to support the react-native runtime.
This library uses node's crypto, buffer (it also uses http, https, fs and path but I can work around these dependencies).
It also uses jwk-to-pem, which uses asm1js, which in turn uses crypto.

     keycloak-nodejs-auth-utils
     |           |         |
     |        buffer jwk-to-pem
      \                    |
       \____________     asm1js
                    \      |
                     \__crypto

There are a couple of options here:

1. Browserify all the dependencies and adapt the library to use them.
This is hackish, but works with minimal effort.

2. Set the code to be dependent on the modules that are necessary (i.e crypto-browserify instead of crypto, buffer npm module instead of node's buffer).
Then adpat asm1js to use crypto-browserify & the npm buffer, then make the library use these dependencies.
This isn't hackish, but takes a lot of effort.

The 2nd approach maybe easier to tackle if I knew how to use webpack aliasing to transform dependencies, but It's not straight-forward and I don't want to waste time on that at the moment. So I chose the 1st approach.
Once I'll wrap my head around webpack , I'll probably be able to do all the woodoo browserify does using webpack.
