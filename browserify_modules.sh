#!/usr/bin/env bash


dir_name="browserify_modules"
bundle_path="../browserified_modules.js"


WHITE='\033[1;37m'
BLUE='\033[0;34m'
RED='\033[0;31m'
GREEN='\033[0;32m'


echo -e "${WHITE}=======================================" &&
echo -e "${BLUE}creating browserified_modules.js bundle"  &&
echo -e "${WHITE}=======================================" &&
echo ""                                                   &&


# make all these woodoo things inside a temporary directory
mkdir ${dir_name} &&
cd    ${dir_name} &&


# generate package.json to install dependencies, then install it
echo '{
  "name": "'${dir_name}'",
  "version": "1.0.0",
  "description": "temporary package",
  "repository": "not.real.repository.git",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}' > package.json                    &&
npm install --save jwk-to-pem        &&
npm install --save crypto-browserify &&
npm install --save buffer            &&


# create the code browserify will recursively bundle
echo "
jwkToPem = require('jwk-to-pem');
crypto   = require('crypto');
buffer   = require('buffer');
" > index.js &&


# generate the bundle & export the modules
echo "let jwkToPem,crypto,buffer;"               > ${bundle_path} &&
browserify index.js                             >> ${bundle_path} &&
echo "module.exports={jwkToPem,crypto,buffer};" >> ${bundle_path} &&


# replace "require" with "require_" to avoid conflicts with react-native's "require"
sed -i '' 's/require/require_/g' ${bundle_path} &&


# get line number of the dependencies list of module number 70 (bn.js), and add "buffer" as dependency
line_number=$(grep -n '],70:' ${bundle_path} | cut -d ":" -f1)  &&
sed -i '' ${line_number}'s/{}/{"buffer": 100}/g' ${bundle_path} &&


echo ""                                                               &&
echo -e "${WHITE}===================================================" &&
echo -e "${GREEN}created browserified_modules.js bundle successfully" ||
echo -e "${RED}failed to create the browserified_modules.js bundle"   &&
echo -e "${WHITE}==================================================="


# clean up. Done whether bundling failed or succeeded. That why there's no && or || after the previous command
cd ../             &&
rm -rf ${dir_name} ||
    echo 'please delete "'${dir_name}'" directory'
